import os, sys

sys.path.append(os.path.expanduser('~/eternity-script/pycparser'))
from pycparser import *

script = r"""
void func ()
{
  x = 1;
}
"""

parser = c_parser.CParser()
ast = parser.parse(script)
print("Before:")
ast.show(offset=2)
assignment = ast.ext[0].body.block_items[0]
assignment.lvalue.name = "y"
assignment.rvalue.value = 2
print("After:")
ast.show(offset=2)